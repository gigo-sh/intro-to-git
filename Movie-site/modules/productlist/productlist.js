import { pagination , rendermovies  } from './pagination.js'
import { getCartItems, Busketlistener } from './cart.js'
import  {homemain} from './../home/homee.js'
import {opensidebar , menulistener} from './sidebar.js'

let movies = [];
let filterarray = [];
function searchmovies() {
    const SearchInput = document.getElementById('search')
    console.log(SearchInput.value)
    let filtermovies = movies.filter(el => el.title.toLowerCase().match(SearchInput.value.toLowerCase()))
    if (SearchInput.value === '') {
        rendermovies(movies, 0, 3)
        pagination(movies, 3)
    } else {
        rendermovies(filtermovies, 0, 3)
        pagination(filtermovies, 3)
    }
};

function filterbycategory(e){
   
    if(e.target.checked == true){
        filterarray.push(e.target.value)  
    }else {
        let temparray = filterarray.filter(el => el !== e.target.value);
        filterarray = temparray;
    }
    let filtermovies = [];
    filterarray.forEach(el => {
        console.log(el)
        let checkedMovie =  movies.filter(movie => movie.genre.toLowerCase() == el.toLowerCase())
        filtermovies.push(...checkedMovie) 
        
    })
    if (filterarray.length == 0) {
        rendermovies(movies, 0, 3)
        pagination(movies, 3)
    } else {
        rendermovies(filtermovies, 0, 3)
        pagination(filtermovies, 3)
    }
    console.log(filterarray)
    console.log(e.target.checked, e.target.value);
    
};

function categorylistener(){
    const categorycheckbox = [...document.getElementsByClassName('movie-genre')]
    categorycheckbox.forEach(checkbox => checkbox.addEventListener('change', (e) => filterbycategory(e)));
};

function closemodal(){
    const backdrop = document.getElementById('buy-modal')
    backdrop.style.display = 'none'
}
function closemodallistener(){
    const backdrop = document.getElementById('buy-modal')
    backdrop.addEventListener('click', closemodal)
}

function getmovies() {
    fetch('http://localhost:3000/movies')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data) {
                movies = data
                rendermovies(movies, 0, 3)
                pagination(movies, 3)
            }
        })
};



function main() {
    function addsearchlistener() {
        const SearchInput = document.getElementById('search')
        SearchInput.addEventListener('keyup', searchmovies)
    }
    Busketlistener()
    categorylistener()
    addsearchlistener()
    getmovies()
    getCartItems()
    closemodallistener()
    menulistener(movies)
};



window.onload = function () {
    main();
    homemain();
}






export function opensidebar(){
    const sidebar = document.getElementsByClassName('Sidemenu')[0]
    sidebar.style.left = 0    
}
export function closesidebar(){
    const sidebar = document.getElementsByClassName('Sidemenu')[0]
    sidebar.style.left = '-300px';    
}
export function menulistener(movies){
    const burgermenu = document.getElementsByClassName('burgermenu')[0]
    burgermenu.addEventListener('click', opensidebar)
    const closebtn = document.getElementsByClassName('sidemenuclose')[0]
    closebtn.addEventListener('click', closesidebar) ;
    const fromInput  = document.getElementById('From');
    fromInput.addEventListener('change', ()=> filterbyprice(movies))
    const toinput = document.getElementById('To')
    toinput.addEventListener('change', ()=> filterbyprice(movies))
}

export function filterbyprice(movies, rendermovies){
    const pricefrom = document.getElementById('From')
    const priceto = document.getElementById('To')
    let filtermovies =[]
    if(pricefrom.value != ""){
        filtermovies= movies.filter(el => el.price >= pricefrom.value)
    }else if(pricefrom.value != "" && priceto.value != ""){
        filtermovies=movies.filter(el => el.price >= pricefrom.value && el.price <= priceto.value)
    }else if(priceto.value != ""){
        filtermovies= movies.filter(el => el.price <= priceto.value)
    }else if(pricefrom.value == "" && priceto.value == ""){
        filtermovies=movies
    }
    rendermovies(filtermovies,0, 3);
    
  
}
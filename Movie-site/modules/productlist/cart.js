export function addcartlistener(movies) {
    const addcartbuttons = [...document.getElementsByClassName('buy')]
    addcartbuttons.forEach(btn => btn.addEventListener('click', () => addCartItem(btn.id, movies)));
};

export function getCartItems(){
    let cartItems = []
    try {
        cartItems = JSON.parse(localStorage.getItem('cartItems') || '[]');
        const Itemcount = document.getElementsByClassName('item-count')[0];
        if (cartItems.length > 0) {
            Itemcount.innerHTML = cartItems.length
        }
        console.log(cartItems)
    } catch (error) {
        return [];
    }
    return cartItems;
};

export function addCartItem(id, moveis){

    let tempCartitem = moveis.filter(el => el.id == id)[0];
    let cartItems = getCartItems();
    cartItems.push(tempCartitem);
    const Itemcount = document.getElementsByClassName('item-count')[0];
    if (cartItems.length > 0) {
        Itemcount.innerHTML = cartItems.length
    };
    return localStorage.setItem('cartItems', JSON.stringify(cartItems));
};

function updatecartitems(id) {
    let boughtitems = JSON.parse(localStorage.getItem('cartItems'));
    console.log('boughtitems', boughtitems);
    let filtereditems = boughtitems.filter(el => el.id != id);
    console.log('filtereditems', filtereditems)
    const Itemcount = document.getElementsByClassName('item-count')[0];
    if (filtereditems.length > 0) {
        Itemcount.innerHTML = filtereditems.length
    } else {
        Itemcount.innerHTML = ""
    };
    localStorage.setItem('cartItems', JSON.stringify(filtereditems))
    rendercartitems(filtereditems)
};

export function clearCartItems(){
    return localStorage.setItem('cartItems', '[]');
};

export function rendercartitems(data) {
    const itemlist = document.getElementById('buy-items-modal')
    itemlist.innerHTML = '';
    let itemsHTML = '';
    data.forEach((item, index) => {
        itemsHTML += `
    <div class="buy-items">
            <p>${item.title}</p>
            <div class="buy-items-right">
                <p>${item.price}</p>
                <p class='delete' id='${item.id}'>X</p>
            </div>
        </div>
    `
    });
    itemlist.insertAdjacentHTML('beforeend', itemsHTML);
    deleteitemlistener()
};

export function Busketlistener() {
    const busketbtn = document.getElementsByClassName('busket')[0];
    busketbtn.addEventListener('click', opencartmodal)
};

function opencartmodal() {
    let cartitems = JSON.parse(localStorage.getItem('cartItems'));
    const modal = document.getElementById('buy-modal');
    modal.style.display = "flex";
    modal.addEventListener('click', (e) => e.stopPropagation());

    rendercartitems(cartitems)
};

function deleteitemlistener() {
    const deletbtn = [...document.getElementsByClassName('delete')]
    deletbtn.forEach(btn => btn.addEventListener('click', () => updatecartitems(btn.id)));
};


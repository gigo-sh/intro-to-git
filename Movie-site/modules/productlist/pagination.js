import {addcartlistener} from './cart.js'
let pages = 0;
let currentPage = 1;



export function pagination(data, dataPerPage) {
    pages = Math.trunc(data.length / dataPerPage);
    if (data.length % dataPerPage > 0) {
        pages += 1;
    }
    const paginationContainer = document.getElementsByClassName('page-container')[0];
    paginationContainer.innerHTML = '';
    let pagesHTML = '<div class="numbers">Prev</div>';
    for (let i = 0; i < pages; i++) {
        if (i == 0) {
            pagesHTML += `<div class="numbers">${i + 1}</div>`
        } else {
            pagesHTML += `<div class="numbers">${i + 1}</div>`
        }
    }
    pagesHTML += '<div class="numbers">Next</div>';
    paginationContainer.insertAdjacentHTML('beforeend', pagesHTML);
    pagein(data, dataPerPage)
};


function pagein(data, dataPerPage) {
    const pageBtn = [...document.getElementsByClassName('numbers')];
    pageBtn.forEach(btn => {
        btn.addEventListener('click', () => {
            if (btn.textContent == 'Prev') {
                if (currentPage - 1 < 1) {
                    return
                }
                currentPage =currentPage - 1;
                const movies = dataPerPage * (currentPage - 1);
                rendermovies(data, movies, movies + dataPerPage);
            } else if (btn.textContent == 'Next') {
                if (+currentPage +1 > pages) {
                    return
                } 
                currentPage = + currentPage + 1;
                const movies = dataPerPage * (currentPage - 1);
                rendermovies(data, movies, movies + dataPerPage);
            }else {
                currentPage = btn.textContent;
                const movies = dataPerPage * (currentPage - 1);
                rendermovies(data, movies, movies + dataPerPage);
            }
            
        })
    });
};

export function rendermovies(products, from , to) {
    const movielist = document.getElementById('productlist')
    movielist.innerHTML ='';
    let productsHTML ='';
    products.slice(from, to).forEach(movie => {
        productsHTML +=`
        <div class="movie-card">
           <div class="movie-img">
               <img src=${movie.img}  alt="">
           </div>     
           <div class="movie-det">
               <p class="title">${movie.title} </p>
               <p class="description">${movie.description}</p>
               <div class="card-footer">
                   <p class="genre">${movie.genre}</p>
                   <p class="price">${movie.price}</p>
                   <button class="buy" id="${movie.id}"> Buy </button>
               </div>
   
           </div>   
       </div>
        `
    });
    movielist.insertAdjacentHTML('beforeend', productsHTML);
    addcartlistener(products)

   } 



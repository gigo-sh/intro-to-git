import { openmodal, closemodal, registerUsername, } from "../auth/reg.js";
import { loginUsename } from "../auth/login.js";

function loginUser() {
    const SigninBtn = document.getElementsByClassName('confirm-btn')[1]
    SigninBtn.addEventListener('click', loginUsename);
};

function login() {
    const loginBtn = document.getElementsByClassName('btn-primary')[0]
    loginBtn.addEventListener('click', () => openmodal(1));
};

function closeRegister() {
    const modal = document.getElementsByClassName('modal')[0];
    const backdrop = document.getElementsByClassName('backdrop')[0];
    const closebtn = document.getElementsByClassName('close-btn')[0];
    modal.addEventListener('click', (e) => e.stopPropagation());
    backdrop.addEventListener('click', () => closemodal(0));
    closebtn.addEventListener('click', () => closemodal(0));
    registerUsername()
};

function registerUser() {
    const signupbtn = document.getElementsByClassName('confirm-btn')[0]
    signupbtn.addEventListener('click', registerUsername);
};
function register() {
    const registerbtn = document.getElementsByClassName('btn-primary')[1]
    registerbtn.addEventListener('click', () => openmodal(0));
};

function closeLogin() {
    const modal = document.getElementsByClassName('modal')[1];
    const backdrop = document.getElementsByClassName('backdrop')[1];
    const closebtn = document.getElementsByClassName('close-btn')[1];
    modal.addEventListener('click', (e) => e.stopPropagation());
    backdrop.addEventListener('click', () => closemodal(1));
    closebtn.addEventListener('click', () => closemodal(1));
}

export function homemain(){
    register();
    registerUser();
    closeRegister()
    login()
    loginUser()
    closeLogin()
}

window.onload = function () {
    homemain()
}




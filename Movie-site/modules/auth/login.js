export function loginUsename() {
    const username = document.getElementById('Username1').value;
    const password = document.getElementById('Password1').value;
    if (username === '' || password === '') {
        return

    }

    fetch('http://localhost:3000/users').then(response => {
        return response.json()
    }).then(users => {
        const User = users.filter(user => user.username === username && user.password === password);
        if (User.length > 0) { 
            loginUser(User)
        }

    })
        
}
export function loginUser(user) {

    localStorage.setItem('logedInUser', JSON.stringify(user[0]));
    location.reload();
}
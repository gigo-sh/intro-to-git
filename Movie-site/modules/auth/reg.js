export function openmodal(indx) {
    let backdrop = document.getElementsByClassName('backdrop')[indx]
    backdrop.style.display = "flex";
}

export function closemodal(indx) {
    let backdrop = document.getElementsByClassName('backdrop')[indx]
    backdrop.style.display = "none";
}
export function registerUsername() {
    const username = document.getElementById('Username').value;
    const password = document.getElementById('Password').value;
    const confirmpassword = document.getElementById('Confirm-password').value;
    if(username === '' || password === '' || confirmpassword === ''){
        return
    }else if(password !== confirmpassword){
        return
    }    
    const data = {username: username, password: password};
    fetch('http://localhost:3000/users').then(response => {
        return response.json()
    }).then(users =>{ 
        if(users.filter(user => user.username === username).length === 0) {
            registerUser(data)
        }

    })
        
}

function registerUser(data) {
    fetch('http://localhost:3000/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then(() => {
        loginUser(data.username, data.password);
    });
}
